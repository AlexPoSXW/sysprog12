#include "csapp.h"
#include <unistd.h>

typedef struct {
	char * buf;
	int n;
	int front;
	int rear;
	sem_t mutex;
	sem_t slots;
	sem_t items;
	int datos_en_buffer;
} sbuf_t;

void sbuf_init(sbuf_t *sp, int n){
	sp->buf = Calloc(n,sizeof(char));
	sp->n = n;
	sp->front = sp->rear = 0;
	Sem_init(&sp->mutex,0,1);
	Sem_init(&sp->slots,0,n);
	Sem_init(&sp->items,0,0);
	sp->datos_en_buffer = 0;
}

void sbuf_deinit(sbuf_t *sp){
	Free(sp->buf);
}

void sbuf_insert(sbuf_t *sp, char item){
	P(&sp->slots);
	P(&sp->mutex);
	sp->buf[(++sp->rear)%(sp->n)] = item;
	sp->datos_en_buffer++;
	V(&sp->mutex);
	V(&sp->items);
}

char sbuf_remove(sbuf_t *sp){
	char item;
	P(&sp->items);
	P(&sp->mutex);
	item = sp->buf[(++sp->front)%(sp->n)];
	sp->datos_en_buffer--;
	V(&sp->mutex);
	V(&sp->slots);
	return item;
}

sbuf_t buffer;
void *thread (void *);
int volatile wey = 1;
int main(int argc, char **argv)
{	
	
	char *filename;
	int fd;
	char c;
	struct stat fileStat;
	pthread_t tid;
	if (argc != 2) {
		printf("usage: %s <filename>\n", argv[0]);
		exit(0);
	}
	filename = argv[1];


	if (stat(filename, &fileStat)<0){	
		printf("Archivo %s no fue encontrado\n",filename);
	}
	else{
		printf("Abriendo archivo %s...\n",filename);
		fd = Open(filename, O_RDONLY, 0);
		printf("Creando el hilo consumidor\n");
		Pthread_create(&tid,NULL,thread,NULL);
		sbuf_init(&buffer,10);
		
		
		while(Read(fd,&c,1)){
			usleep(50);
			sbuf_insert(&buffer,c);
			//printf("Write on buffer: %c\n",c);
		}			
		Close(fd);
		wey = 0;
		Pthread_join(tid,NULL);
		
	}
	
	return 0;
}


void *thread(void *args){
	char c;
	while(buffer.datos_en_buffer!=0||wey==1){
		sleep(1);
		c = sbuf_remove(&buffer);
		printf("Read: %c\n",c);
	}
	exit(0);
}
